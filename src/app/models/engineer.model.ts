import { User } from "./user.model";
import { Deserializable } from "./deserializable.model";
import { City } from "./city.model";
import { Region } from "./region.model";

export class Engineer implements Deserializable {

    public id: number;
    public user: User;
    public work_region: Region;
    public work_cities: City[];
    public work_cities_ids: string;

    constructor(
        id?: number,
        user?: User,
        work_region?: Region,
        work_cities: City[] = [],
        work_cities_ids: string = ""
    ) {
        this.id = id;
        this.user = user;
        this.work_region = work_region;
        this.work_cities = work_cities;
        this.work_cities_ids = work_cities_ids;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.work_region = new Region().deserialize(input.work_region);
        for (let city of input.cities) {
            this.work_cities.push(new City().deserialize(city));
        }
        return this;
    }

}