import { User } from "./user.model";
import { Deserializable } from "./deserializable.model";
import { City } from "./city.model";
import { Region } from "./region.model";
import { Category } from "./category.model";

export class Product implements Deserializable {

    public id: number;
    public category: Category;
    public name: string;
    public picture: string;
    public description: string;
    public unit_price: number;
    public quantity: number;
    public region: Region;
    public status: boolean;
    public creation_date: Date;

    constructor(
        id?: number,
        category?: Category,
        name?: string,
        picture?: string,
        description?: string,
        unit_price?: number,
        quantity?: number,
        region?: Region,
        status: boolean = true,
        creation_date?: Date
    ) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.picture = picture;
        this.description = description;
        this.unit_price = unit_price;
        this.quantity = quantity;
        this.region = region;
        this.status = status;
        this.creation_date = creation_date;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.category = new Category().deserialize(input.category);
        this.region = new Region().deserialize(input.region);
        return this;
    }

}