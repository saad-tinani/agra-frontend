import { Deserializable } from "./deserializable.model";
import { User } from "./user.model";

export class SigninResponse implements Deserializable {

    public status: boolean;
    public user: User;

    constructor(
        status: boolean = false,
        user: User = null
    ) {
        this.status = status;
        this.user = user;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.user = new User().deserialize(input.user);
        return this;
    }

}