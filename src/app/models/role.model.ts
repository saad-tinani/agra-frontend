import { Deserializable } from "./deserializable.model";

export class Role {

    public id: number;
    public name: string;
    public privileges_management: boolean;
    public directors_management: boolean;
    public engineers_management: boolean;
    public clients_management: boolean;
    public products_management: boolean;
    public prices_management: boolean;
    public sales_management: boolean;
    public orders_management: boolean;
    public creation_date: Date;

    constructor(
        id?: number,
        name?: string,
        privileges_management?: boolean,
        directors_management?: boolean,
        engineers_management?: boolean,
        clients_management?: boolean,
        products_management?: boolean,
        prices_management?: boolean,
        sales_management?: boolean,
        orders_management?: boolean,
        creation_date?: Date
    ) {
        this.id = id;
        this.name = name;
        this.privileges_management = privileges_management;
        this.directors_management = directors_management;
        this.engineers_management = engineers_management;
        this.clients_management = clients_management;
        this.products_management = products_management;
        this.prices_management = prices_management;
        this.sales_management = sales_management;
        this.orders_management = orders_management;
        this.creation_date = creation_date;
    }

}