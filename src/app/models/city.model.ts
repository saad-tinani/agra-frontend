import { Deserializable } from "./deserializable.model";
import { Region } from "./region.model";

export class City implements Deserializable {

    public id: number;
    public name: string;
    public region: Region;

    constructor(
        id?: number,
        name?: string,
        region?: Region
    ) {
        this.id = id;
        this.name = name;
        this.region = region;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.region = new Region().deserialize(input.region);
        return this;
    }
    
}