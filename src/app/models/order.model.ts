import { Deserializable } from "./deserializable.model";
import { Product } from "./product.model";
import { User } from "./user.model";

export class Order implements Deserializable {

    public id: number;
    public creation_date: Date;
    public reference: string;
    public status: number;
    public client: User;

    constructor(
        status?: number,
        reference?: string,
        client?: User,
        id?: number,
        creation_date?: Date
        
    ) {
        this.id = id;
        this.creation_date = creation_date;
        this.reference = reference;
        this.status = status;
        this.client = client;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.client = new User().deserialize(input.client);
        return this;
    }
    
}