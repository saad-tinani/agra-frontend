import { Deserializable } from "./deserializable.model";
import { City } from "./city.model";
import { Role } from "./role.model";

export class User implements Deserializable {

    public id: number;
    public avatar: string;
    public username: string;
    public email: string;
    public phone: string;
    public first_name: string;
    public last_name: string;
    public cin: string;
    public address: string;
    public city: City;
    public zip_code: number;
    public role: Role;
    public status: boolean;
    public creation_date: Date;

    constructor(
        id?: number,
        avatar?: string,
        username?: string,
        email?: string,
        phone?: string,
        first_name?: string,
        last_name?: string,
        cin?: string,
        address?: string,
        city?: City,
        zip_code?: number,
        role?: Role,
        status: boolean = true,
        creation_date?: Date
    ) {
        this.id = id;
        this.avatar = avatar;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.first_name = first_name;
        this.last_name = last_name;
        this.cin = cin;
        this.address = address;
        this.city = city;
        this.zip_code = zip_code;
        this.role = role;
        this.status = status;
        this.creation_date = creation_date;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.city = new City().deserialize(input.city);
        return this;
    }

    getFullName(): String {
        return this.first_name + ' ' + this.last_name;
    }

    getFullAddress(): String {
        return this.address + ', ' + this.city + ' - ' + this.zip_code;
    }

}