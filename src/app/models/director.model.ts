import { User } from "./user.model";
import { Deserializable } from "./deserializable.model";
import { Region } from "./region.model";

export class Director implements Deserializable {

    public id: number;
    public user: User;
    public work_region: Region;

    constructor(id?: number,
                user?: User,
                work_region?: Region) {
        this.id = id;
        this.user = user;
        this.work_region = work_region;
    }

    getFullName(): String {
        return this.user.getFullName();
    }

    getFullAddress(): String {
        return this.user.getFullAddress();
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.user = new User().deserialize(input.user);
        this.work_region = new Region().deserialize(input.work_region);
        return this;
    }

}