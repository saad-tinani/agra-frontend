import { Deserializable } from "./deserializable.model";
import { Product } from "./product.model";
import { Order } from "./order.model";

export class OrderDetail implements Deserializable {

    public id: number;
    public negociation_unit_price: number;
    public quantity: number;
    public order: Order;
    public product: Product;

    constructor(
        id?: number,
        negociation_unit_price?: number,
        quantity?: number,
        order?: Order,
        product?: Product
    ) {
        this.id = id;
        this.negociation_unit_price = negociation_unit_price;
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        this.order = new Order().deserialize(input.order);
        this.product = new Product().deserialize(input.product);
        return this;
    }
    
}