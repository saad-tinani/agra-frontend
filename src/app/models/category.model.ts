import { Deserializable } from "./deserializable.model";

export class Category implements Deserializable {

    public id: number;
    public name: string;
    public description: string;
    public position: number;
    public status: boolean;
    public creation_date: Date;

    constructor(
        id?: number,
        name?: string,
        description?: string,
        position?: number,
        status?: boolean,
        creation_date?: Date
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.position = position;
        this.status = status;
        this.creation_date = creation_date;
    }

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
    
}