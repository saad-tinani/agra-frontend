import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
	FormBuilder,
	FormGroup,
	Validators
} from '@angular/forms';
import { Region } from 'src/app/models/region.model';
import { RegionsService } from 'src/app/services/regions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Product } from 'src/app/models/product.model';
import { ProductsService } from 'src/app/services/products.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/app/models/category.model';

@Component({
	selector: 'app-new-product',
	templateUrl: './new-product.component.html',
	styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

	validateForm: FormGroup;

	private product: Product = new Product();

	regionId: number = 0;
	categoryId: number = 0;
	private regions: Region[] = [];
	private categories: Category[] = [];

	constructor(private _location: Location,
		private _loader: NgxSpinnerService,
		private _productsService: ProductsService,
		private _regionsService: RegionsService,
		private _categoriesService: CategoriesService,
		private _formBuilder: FormBuilder) {

		this.validateForm = this._formBuilder.group({
			name: ['', [Validators.required]],
			description: ['', [Validators.required]],
			picture: ['', []],
			unit_price: ['', [Validators.required]],
			quantity: ['', [Validators.required]],
			region: ['', [Validators.required]],
			category: ['', [Validators.required]],
			status: [true, Validators.required]
		});

	}

	ngOnInit() {
		// Get regions and from the server
		this.getRegionsFromServer();
		this.getCategoriesFromServer();
	}

	goBack(): void {
		this._location.back();
	}

	getRegions(): Region[] {
		return this.regions;
	}

	getCategories(): Category[] {
		return this.categories;
	}

	getRegionsFromServer(): void {
		// Subscribe to the regions service
		this._regionsService.getRegions().subscribe(
			(response) => {
				this.regions = response.map(region => {
					return new Region(
						region.id,
						region.name
					);
				});
			}, (error) => {
				console.log("getRegionsFromServer() failed: ", error);
			}
		);
	}

	getCategoriesFromServer(): void {
		// Subscribe to the categories service
		this._categoriesService.getCategories().subscribe(
			(response) => {
				this.categories = response.map(category => {
					return new Category(
						category.id,
						category.name,
						category.description,
						category.position,
						category.status,
						category.creation_date
					);
				});
			}, (error) => {
				console.log("getCategoriesFromServer() failed: ", error);
			}
		);
	}

	createProduct(product: Product): void {
		// Subscribe to the users service
		this._productsService.createProduct(product).subscribe(
			(prod) => {
				this.goBack();
				this._loader.hide();
			}, (error) => {
				console.log("createProduct() failed: ", error);
				this._loader.hide();
			}
		);
	}

	submitForm = ($event, value) => {

		// Validation
		$event.preventDefault();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}

		// Show the loader
		this._loader.show();

		// Set Engineer avatar
		//this.user.avatar = this.selectedFiles.item(0).name;

		// Set Product region
		for (let i = 0; i < this.regions.length; i++) {
			if (this.regions[i].id == this.regionId) {
				this.product.region = new Region(this.regions[i].id, this.regions[i].name);
				break;
			}
		}

		// Set Product category
		for (let i = 0; i < this.categories.length; i++) {
			if (this.categories[i].id == this.categoryId) {
				this.product.category = new Category(this.categories[i].id, this.categories[i].name, this.categories[i].description, this.categories[i].position, this.categories[i].status, this.categories[i].creation_date);
				break;
			}
		}

		// Set product picture
		this.product.picture = "product.png";

		// Create the product
		this.createProduct(this.product);

	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
	}

	// Upload the engineer's avatar
	selectedFiles: FileList;
	currentFileUpload: File;
	progress: { percentage: number } = { percentage: 0 };

	selectFile(event) {
		this.selectedFiles = event.target.files;
	}

	/*upload() {
	  this.progress.percentage = 0;
  
	  console.log(this.selectedFiles)
  
	  this.currentFileUpload = this.selectedFiles.item(0);
  
	  this._engineersService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
		if (event.type === HttpEventType.UploadProgress) {
		  this.progress.percentage = Math.round(100 * event.loaded / event.total);
		} else if (event instanceof HttpResponse) {
		  console.log('File is completely uploaded!');
		}
	  });
  
	  this.selectedFiles = undefined;
	}*/

}
