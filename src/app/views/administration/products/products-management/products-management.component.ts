import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductsFilterPipe } from 'src/app/filters/products-filters/products-filter.pipe';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { ProductsService } from 'src/app/services/products.service';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-products-management',
	templateUrl: './products-management.component.html',
	styleUrls: ['./products-management.component.css']
})
export class ProductsManagementComponent implements OnInit {

	// Productd table
	private products: Product[];

	// Search
	private searchText: string;
	private productsFilterPipe = new ProductsFilterPipe();

	constructor(
		private _location: Location,
		private _productsService: ProductsService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService
	) { }

	ngOnInit() {
		this.getProducts();
	}

	goBack(): void {
		this._location.back();
	}

	getProductsNumber(): number {
		return this.productsFilterPipe.transform(this.products, this.searchText).length;
	}

	getProducts(): void {
		// Show the loader
		this._loader.show();
		if (this._authService.getUser().role.id == 1) {
			// Subscribe to the service
			this._productsService.getProducts().subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 2) {
			// Subscribe to the service
			this._productsService.getRegionProducts(this._authService.getDirector().work_region.id).subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 3) {
			// Subscribe to the service
			this._productsService.getRegionProducts(this._authService.getEngineer().work_region.id).subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		}

	}

	changeProductStatus(product: Product) {
		// Show the loader
		this._loader.show();
		// Change the engineer status
		product.status = !product.status;
		// Subscribe to the service
		this._productsService.updateProduct(product).subscribe(
			() => {
				// Update engineers table
				this.products.forEach(element => {
					element = element.id == product.id ? product : element;
				});
				this._loader.hide();
			},
			(error) => {
				console.log("changeProductStatus() failed: ", error);
				this._loader.hide();
			}
		);
	}

	changeProductStatusConfirmation(product: Product, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.changeProductStatus(product),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
