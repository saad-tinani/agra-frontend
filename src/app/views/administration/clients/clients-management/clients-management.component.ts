import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ClientsFilterPipe } from 'src/app/filters/clients-filters/clients-filter.pipe';
import { ClientsService } from 'src/app/services/clients.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { User } from 'src/app/models/user.model';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-clients-management',
	templateUrl: './clients-management.component.html',
	styleUrls: ['./clients-management.component.css']
})
export class ClientsManagementComponent implements OnInit {

	// Clients table
	private clients: User[];

	// Search
	private searchText: string;
	private clientsFilterPipe = new ClientsFilterPipe();

	constructor(private _location: Location,
		private _clientsService: ClientsService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService) { }

	ngOnInit() {

		// Get clients from the server
		this.getClients();

	}

	goBack(): void {
		this._location.back();
	}

	getClientsNumber(): number {
		return this.clientsFilterPipe.transform(this.clients, this.searchText).length;
	}

	getClients(): void {
		// Show the loader
		this._loader.show();
		if (this._authService.getUser().role.id == 1) {
			// Subscribe to the service
			this._clientsService.getClients().subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getClients() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 2) {
			// Subscribe to the service
			this._clientsService.getDirectorClients(this._authService.getDirector().work_region.id).subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getDirectorClients() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 3) {
			// Subscribe to the service
			var cities_ids: number[] = this._authService.getEngineer().work_cities_ids.split(",").map(Number);
			this._clientsService.getEngineerClients(cities_ids).subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getEngineerClients() failed: ", error);
					this._loader.hide();
				}
			);
		}

	}

	updateClient(client: User): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._clientsService.updateClient(client).subscribe(
			() => {
				this._loader.hide();
				this.goBack();
			},
			(error) => {
				console.log("updateClient() failed: ", error);
			}
		);
	}

	changeClientStatus(client: User) {
		// Show the loader
		this._loader.show();
		// Change the client status
		client.status = !client.status;
		// Subscribe to the service
		this._clientsService.updateClient(client).subscribe(
			() => {
				// Update clients table
				this.clients.forEach(element => {
					element = element.id == client.id ? client : element;
				});
				this._loader.hide();
			},
			(error) => {
				console.log("changeClientStatus() failed: ", error);
			}
		);

	}

	changeClientStatusConfirmation(client: User, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.changeClientStatus(client),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
