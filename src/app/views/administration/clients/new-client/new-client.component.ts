import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
	FormBuilder,
	FormControl,
	FormGroup,
	ValidationErrors,
	Validators
} from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { City } from 'src/app/models/city.model';
import { Region } from 'src/app/models/region.model';
import { NzMessageService, UploadFile } from 'ng-zorro-antd';
import { User } from 'src/app/models/user.model';
import { Role } from 'src/app/models/role.model';
import { CitiesService } from 'src/app/services/cities.service';
import { RegionsService } from 'src/app/services/regions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ClientsService } from 'src/app/services/clients.service';

@Component({
	selector: 'app-new-client',
	templateUrl: './new-client.component.html',
	styleUrls: ['./new-client.component.css']
})
export class NewClientComponent implements OnInit {

	validateForm: FormGroup;

	private client: User = new User();

	cityId: number;
	private cities: City[] = [];
	private regions: Region[] = [];

	constructor(private _location: Location,
		private _loader: NgxSpinnerService,
		private _usersService: ClientsService,
		private _citiesService: CitiesService,
		private _regionsService: RegionsService,
		private _formBuilder: FormBuilder,
		private msg: NzMessageService) {

		this.validateForm = this._formBuilder.group({
			first_name: ['', [Validators.required]],
			last_name: ['', [Validators.required]],
			avatar: ['', []],
			cin: ['', [Validators.required], [this.cinAsyncValidator]],
			address: ['', [Validators.required]],
			city: ['', [Validators.required]],
			zip_code: ['', [Validators.required]],
			phone: ['', [Validators.required]],
			username: ['', [Validators.required], [this.usernameAsyncValidator]],
			email: ['', [Validators.email, Validators.required], [this.emailAsyncValidator]],
			password: ['', [Validators.required]],
			confirm: ['', [this.confirmValidator]],
			status: [true, Validators.required]
		});

	}

	ngOnInit() {

		// Get cities && regions from the server
		this.getCitiesFromServer();
		this.getRegionsFromServer();

	}

	goBack(): void {
		this._location.back();
	}

	getCities(): City[] {
		return this.cities;
	}

	getRegions(): Region[] {
		return this.regions;
	}

	getCitiesFromServer(): void {
		// Subscribe to the engineers service
		this._citiesService.getCities().subscribe(
			(response) => {
				this.cities = response.map(city => {
					return new City(
						city.id,
						city.name,
						new Region(city.region.id, city.region.name)
					);
				});
			}, (error) => {
				console.log("getCities() failed: ", error);
			}
		);
	}

	getRegionsFromServer(): void {
		// Subscribe to the engineers service
		this._regionsService.getRegions().subscribe(
			(response) => {
				this.regions = response.map(region => {
					return new Region(
						region.id,
						region.name
					);
				});
			}, (error) => {
				console.log("getRegions() failed: ", error);
			}
		);
	}

	cinAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkCin(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	usernameAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkUsername(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	emailAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkEmail(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	validateConfirmPassword(): void {
		setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
	}

	confirmValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
	}

	createClient(client: User): void {

		// Subscribe to the users service
		this._usersService.createClient(client).subscribe(
			(user) => {
				this.goBack();
				this._loader.hide();
			}, (error) => {
				console.log("createClient() failed: ", error);
				this._loader.hide();
			}
		);

	}

	submitForm = ($event, value) => {

		// Validation
		$event.preventDefault();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}

		// Show the loader
		this._loader.show();

		// Set Engineer avatar
		//this.user.avatar = this.selectedFiles.item(0).name;

		// Set client's role
		this.client.role = new Role(4);

		// Set client's city
		this.client.city = new City(this.cityId);

		// Set client infos
		this.client = this.client;

		// Create the engineer
		this.createClient(this.client);

	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
	}

	selectedFiles: FileList;
	currentFileUpload: File;
	progress: { percentage: number } = { percentage: 0 };

	selectFile(event) {
		this.selectedFiles = event.target.files;
	}

	/*upload() {
		this.progress.percentage = 0;

		console.log(this.selectedFiles)

		this.currentFileUpload = this.selectedFiles.item(0);

		this._engineersService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
			if (event.type === HttpEventType.UploadProgress) {
				this.progress.percentage = Math.round(100 * event.loaded / event.total);
			} else if (event instanceof HttpResponse) {
				console.log('File is completely uploaded!');
			}
		});

		this.selectedFiles = undefined;
	}*/

}
