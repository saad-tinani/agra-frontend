import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Location } from '@angular/common';
import { ClientsService } from 'src/app/services/clients.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-client-details',
	templateUrl: './client-details.component.html',
	styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

	private client: User;

	constructor(private _route: ActivatedRoute,
				private _location: Location,
				private _clientsService: ClientsService,
				private _loader: NgxSpinnerService) { }

	ngOnInit() {

		// Get client from the server
		this.getClient();

	}

	goBack(): void {
		this._location.back();
	}

	getClient(): void {
		// Show the loader
		this._loader.show();
		// Get the client id from he url
		const client_id: number = parseInt(this._route.snapshot.paramMap.get('id'));
		// Subscribe to the service
		this._clientsService.getClient(client_id).subscribe(
			(response) => {
				this.client = new User(
										response.id,
										response.avatar,
										response.username,
										response.email,
										response.phone,
										response.first_name,
										response.last_name,
										response.cin,
										response.address,
										response.city,
										response.zip_code,
										response.role,
										response.status,
										response.creation_date
									);
				this._loader.hide();
			}, (error) => {
				console.log("getClient() failed: ", error);
			}
		);
	}

	updateClient(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._clientsService.updateClient(this.client).subscribe(
			() => {
				this._loader.hide();
				this.goBack();
			},
			(error) => {
				console.log("updateClient() failed: ", error);
			}
		);
	}

}
