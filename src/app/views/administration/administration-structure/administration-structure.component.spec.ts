import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationStructureComponent } from './administration-structure.component';

describe('AdministrationStructureComponent', () => {
  let component: AdministrationStructureComponent;
  let fixture: ComponentFixture<AdministrationStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrationStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrationStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
