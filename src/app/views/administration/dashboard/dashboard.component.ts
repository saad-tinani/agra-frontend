import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Product } from 'src/app/models/product.model';
import { Order } from 'src/app/models/order.model';
import { ClientsService } from 'src/app/services/clients.service';
import { AuthService } from 'src/app/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProductsService } from 'src/app/services/products.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

	private clients: User[] = [];
	private products: Product[] = [];
	private orders: Order[] = [];

	public constructor(private _clientsService: ClientsService,
		private _productsService: ProductsService,
		private _ordersService: OrdersService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService) {
		
	}

	public ngOnInit(): any {
		
		this.getOrdersFromServer();
		this.getProductsFromServer();
		this.getClientsFromServer();	

	}

	getRoleId(): number {
		return this._authService.getUser().role.id;
	}

	getNewOrdersNumber(): number {
		return this.orders.filter(order => { return order.status == 1; }).length;
	}

	getOrdersNumber(): number {
		return this.orders.length;
	}

	getValidatedOrdersNumber(): number {
		return this.orders.filter(order => { return order.status == 3; }).length;
	}

	getPendingOrdersNumber(): number {
		return this.orders.filter(order => { return order.status == 2; }).length;
	}

	getCanceledOrdersNumber(): number {
		return this.orders.filter(order => { return order.status == 0; }).length;
	}

	getProductsNumber(): number {
		return this.products.length;
	}

	getClientsNumber(): number {
		return this.clients.length;
	}

	getOrdersFromServer(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._ordersService.getOrders().subscribe(
			(response) => {
				console.log("order: ", response)
				this.orders = response.map(order => {
					return new Order(
						order.status,
						order.reference,
						order.client,
						order.id,
						order.creation_date
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getOrdersFromServer() failed: ", error);
				this._loader.hide();
			}
		);
	}

	getProductsFromServer(): void {
		// Show the loader
		this._loader.show();
		if (this._authService.getUser().role.id == 1) {
			// Subscribe to the service
			this._productsService.getProducts().subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 2) {
			// Subscribe to the service
			this._productsService.getRegionProducts(this._authService.getDirector().work_region.id).subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 3) {
			// Subscribe to the service
			this._productsService.getRegionProducts(this._authService.getEngineer().work_region.id).subscribe(
				(response) => {
					this.products = response.map(product => {
						return new Product(
							product.id,
							product.category,
							product.name,
							product.picture,
							product.description,
							product.unit_price,
							product.quantity,
							product.region,
							product.status,
							product.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getProducts() failed: ", error);
					this._loader.hide();
				}
			);
		}

	}

	getClientsFromServer(): void {
		// Show the loader
		this._loader.show();
		if (this._authService.getUser().role.id == 1) {
			// Subscribe to the service
			this._clientsService.getClients().subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getClients() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 2) {
			// Subscribe to the service
			this._clientsService.getDirectorClients(this._authService.getDirector().work_region.id).subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getDirectorClients() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 3) {
			// Subscribe to the service
			var cities_ids: number[] = this._authService.getEngineer().work_cities_ids.split(",").map(Number);
			this._clientsService.getEngineerClients(cities_ids).subscribe(
				(response) => {
					this.clients = response.map(client => {
						return new User(
							client.id,
							client.avatar,
							client.username,
							client.email,
							client.phone,
							client.first_name,
							client.last_name,
							client.cin,
							client.address,
							client.city,
							client.zip_code,
							client.role,
							client.status,
							client.creation_date
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getEngineerClients() failed: ", error);
					this._loader.hide();
				}
			);
		}

	}
	


}
