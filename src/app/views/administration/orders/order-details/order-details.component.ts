import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
import { OrdersService } from 'src/app/services/orders.service';
import { Location } from '@angular/common';
import { NzModalService } from 'ng-zorro-antd';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-order-details',
	templateUrl: './order-details.component.html',
	styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

	// Items table
	private order_id: number = parseInt(this._route.snapshot.paramMap.get('id'));
	private items: OrderDetail[] = [];

	constructor(private _location: Location,
		private _route: ActivatedRoute,
		private _ordersService: OrdersService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService) { }

	ngOnInit() {

		// Get order items from server
		this.getItemsFromServer();

	}

	getItemsNumber(): number {
		return this.items.length;
	}

	getItems(): OrderDetail[] {
		return this.items;
	}

	getPrixHT(): number {

		var total: number = 0;

		if(this.items.length > 0) {
			this.items.forEach(item => {
				total += item.quantity * item.negociation_unit_price;
			});
		}

		return total;

	}

	// Get TTC
	getPrixTTC(): number {

		var total: number = 0;

		if(this.items.length > 0) {
			this.items.forEach(item => {
				total += item.quantity * item.negociation_unit_price;
			});
		}

		total += total*0.2;

		return total;

	}
	
	// Get total price
	getPrixTVA(): number {

		var total: number = 0;

		if(this.items.length > 0) {
			this.items.forEach(item => {
				total += item.quantity * item.negociation_unit_price;
			});
		}

		return total*0.2;

	}

	getItemsFromServer(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._ordersService.getOrderItems(this.order_id).subscribe(
			(response) => {
				console.log("order: ", response)
				this.items = response.map(item => {
					return new OrderDetail(
						item.id,
						item.negociation_unit_price,
						item.quantity,
						item.order,
						item.product
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getOrdersFromServer() failed: ", error);
				this._loader.hide();
			}
		);
	}

	goBack(): void {
		this._location.back();
	}

	validateOrder() {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._ordersService.updateOrderItems(this.items).subscribe(
			(response) => {
				this.changeOrderStatus(2);
				this._loader.hide();
			}, (error) => {
				console.log("updateOrderItems() failed: ", error);
				this._loader.hide();
			}
		);
	}

	changeOrderStatus(new_status: number) {
		// Show the loader
		this._loader.show();
		// Change the order status
		this.items[0].order.status = new_status;
		// Subscribe to the service
		this._ordersService.updateOrder(this.items[0].order).subscribe(
			() => {
				this.goBack();
				this._loader.hide();
			},
			(error) => {
				console.log("changeOrderStatus() failed: ", error);
				this._loader.hide();
			}
		);
	}

	getOrderId(): number {
		if(this.items[0])
			return this.items[0].order.id;
	}

	getOrderReference(): string {
		if(this.items[0])
			return this.items[0].order.reference;
	}

	getOrderStatus(): number {
		if(this.items[0])
			return this.items[0].order.status;
	}

}
