import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { OrdersFilterPipe } from 'src/app/filters/orders-filters/orders-filter.pipe';
import { EngineersService } from 'src/app/services/engineers.service';
import { ClientsService } from 'src/app/services/clients.service';
import { AuthService } from 'src/app/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { Location } from '@angular/common';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
	selector: 'app-orders-management',
	templateUrl: './orders-management.component.html',
	styleUrls: ['./orders-management.component.css']
})
export class OrdersManagementComponent implements OnInit {

	// Orders table
	private orders: Order[];

	// Search
	private searchText: string;
	private ordersFilterPipe = new OrdersFilterPipe();

	constructor(private _location: Location,
		private _ordersService: OrdersService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService) { }

	ngOnInit() {

		// Get orders from the server
		this.getOrdersFromServer();

	}

	goBack(): void {
		this._location.back();
	}

	getOrdersNumber(): number {
		return this.ordersFilterPipe.transform(this.orders, this.searchText).length;
	}

	getOrders(): Order[] {
		return this.orders.sort((a, b) => {
			if (a.id < b.id)
				return 1;
			else
				return -1;
		});
	}

	getOrdersFromServer(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._ordersService.getOrders().subscribe(
			(response) => {
				console.log("order: ", response)
				this.orders = response.map(order => {
					return new Order(
						order.status,
						order.reference,
						order.client,
						order.id,
						order.creation_date
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getOrdersFromServer() failed: ", error);
				this._loader.hide();
			}
		);
	}

	changeOrderStatus(order: Order) {
		// Show the loader
		this._loader.show();
		// Change the order status
		order.status = order.status == 0 ? 1 : 0;
		// Subscribe to the service
		this._ordersService.updateOrder(order).subscribe(
			() => {
				// Update order table
				this.orders.forEach(element => {
					element = element.id == order.id ? order : element;
				});
				this._loader.hide();
			},
			(error) => {
				console.log("changeOrderStatus() failed: ", error);
				this._loader.hide();
			}
		);
	}

	changeOrderStatusConfirmation(order: Order, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.changeOrderStatus(order),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
