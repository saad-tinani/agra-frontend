import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegesManagementComponent } from './privileges-management.component';

describe('PrivilegesManagementComponent', () => {
  let component: PrivilegesManagementComponent;
  let fixture: ComponentFixture<PrivilegesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
