import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { RolesService } from 'src/app/services/roles.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { Location } from '@angular/common';
import { RolesFilterPipe } from 'src/app/filters/roles-filters/roles-filter.pipe';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-privileges-management',
	templateUrl: './privileges-management.component.html',
	styleUrls: ['./privileges-management.component.css']
})
export class PrivilegesManagementComponent implements OnInit {

	// Roles table
	private roles: Role[];

	// Search
	private searchText: string;
	private rolesFilterPipe = new RolesFilterPipe();

	constructor(private _authService: AuthService,
		private _location: Location,
		private _rolesService: RolesService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService
	) { }

	ngOnInit() {
		this.getRoles();
	}

	goBack(): void {
		this._location.back();
	}

	getRolesNumber(): number {
		return this.rolesFilterPipe.transform(this.roles, this.searchText).length;
	}

	getRoles(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._rolesService.getRoles().subscribe(
			(response) => {
				this.roles = response.map(role => {
					return new Role(
						role.id,
						role.name,
						role.privileges_management,
						role.directors_management,
						role.engineers_management,
						role.clients_management,
						role.products_management,
						role.prices_management,
						role.sales_management,
						role.orders_management,
						role.creation_date
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getRoles() failed: ", error);
			}
		);
	}

	updateRole(role: Role): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._rolesService.updateRole(role).subscribe(
			() => {
				// Update engineers table
				this.roles.forEach(element => {
					element = element.id == role.id ? role : element;
				});

				var _user: User = this._authService.getUser();
				_user.role = new Role(
					role.id,
					role.name,
					role.privileges_management,
					role.directors_management,
					role.engineers_management,
					role.clients_management,
					role.products_management,
					role.prices_management,
					role.sales_management,
					role.orders_management,
					role.creation_date
				);

				this._authService.setUser(_user);

				this._loader.hide();
			},
			(error) => {
				console.log("updateRole() failed: ", error);
			}
		);
	}

	updateRoleConfirmation(role: Role, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.updateRole(role),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
