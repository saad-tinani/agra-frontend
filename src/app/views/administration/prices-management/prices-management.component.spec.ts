import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesManagementComponent } from './prices-management.component';

describe('PricesManagementComponent', () => {
  let component: PricesManagementComponent;
  let fixture: ComponentFixture<PricesManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricesManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
