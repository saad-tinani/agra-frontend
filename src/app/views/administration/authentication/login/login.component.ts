import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	constructor(private _authService: AuthService,
				private _router: Router,
				private _loader: NgxSpinnerService) { }

	ngOnInit() {
	}

	// Submit login form
	onSubmit(event) {

		event.preventDefault();

		// Show the loader
		this._loader.show();

		// Target element
		const target = event.target;

		// Get Email and Password values
		const email = target.querySelector('#email').value;
		const password = target.querySelector('#password').value;

		if (email && password) {

			var _user: any = { "email": email, "password": password };

			this._authService.signIn(_user).subscribe(
				(user) => {

					if (user != null) {

						this._authService.setUser(user);

						switch (user.role.id) {

							case 1:
								this._authService.SetIsLoggedIn(true);
								this._router.navigate(["/"]);
								this._loader.hide();
								break;

							case 2:
								this._authService.getDirectorFromServer(user.id).subscribe(
									(director) => {

										this._authService.SetIsLoggedIn(true);

										this._authService.setDirector(director);

										this._router.navigate(["/"]);
										
										this._loader.hide();

									}, (error) => {
										// Catch the client side error
										console.log("login() failed: ", error);
									}
								);
								break;

							case 3:
								this._authService.getEngineerFromServer(user.id).subscribe(
									(engineer) => {

										this._authService.SetIsLoggedIn(true);

										this._authService.setEngineer(engineer);

										this._router.navigate(["/"]);

										this._loader.hide();

									}, (error) => {
										// Catch the client side error
										console.log("login() failed: ", error);
									}
								);
								break;

							case 4:
								this._authService.SetIsLoggedIn(true);
								this._router.navigate(["/"]);
								this._loader.hide();
								break;

							default:
								this._loader.hide();
								break;
						}

					} else {
						this._loader.hide();
						alert("Your email and/or password are incorrect !");
					}

				}, (error) => {
					// Catch the client side error
					console.log("login() failed: ", error);
				}
			);

		}

	}

}
