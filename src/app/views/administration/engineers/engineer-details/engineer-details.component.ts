import { Component, OnInit } from '@angular/core';
import { Engineer } from 'src/app/models/engineer.model';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { EngineersService } from 'src/app/services/engineers.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/models/user.model';
import {
	FormBuilder,
	FormControl,
	FormGroup,
	ValidationErrors,
	Validators
} from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { City } from 'src/app/models/city.model';
import { Region } from 'src/app/models/region.model';
import { ClientsService } from 'src/app/services/clients.service';
import { CitiesService } from 'src/app/services/cities.service';
import { RegionsService } from 'src/app/services/regions.service';
import { Role } from 'src/app/models/role.model';
import { HttpResponse, HttpEventType } from '@angular/common/http';

@Component({
	selector: 'app-engineer-details',
	templateUrl: './engineer-details.component.html',
	styleUrls: ['./engineer-details.component.css']
})
export class EngineerDetailsComponent implements OnInit {

	validateForm: FormGroup;

	private user: User = new User();
	private engineer: Engineer = new Engineer();

	cityId: string;
	regionId: string;
	workCitiesIds: string[];
	private cities: City[] = [];
	private workCities: City[] = [];
	private regions: Region[] = [];

	constructor(private _route: ActivatedRoute,
		private _location: Location,
		private _engineersService: EngineersService,
		private _usersService: ClientsService,
		private _citiesService: CitiesService,
		private _regionsService: RegionsService,
		private _formBuilder: FormBuilder,
		private _loader: NgxSpinnerService) {

		this.validateForm = this._formBuilder.group({
			first_name: ['', [Validators.required]],
			last_name: ['', [Validators.required]],
			avatar: ['', []],
			cin: ['', [Validators.required], [this.cinAsyncValidator]],
			address: ['', [Validators.required]],
			city: ['', [Validators.required]],
			region: ['', [Validators.required]],
			work_cities: ['', [Validators.required]],
			zip_code: ['', [Validators.required]],
			phone: ['', [Validators.required]],
			username: ['', [Validators.required], [this.usernameAsyncValidator]],
			email: ['', [Validators.email, Validators.required], [this.emailAsyncValidator]],
			password: ['', [Validators.required]],
			confirm: ['', [this.confirmValidator]],
			status: [true, Validators.required]
		});

	}

	ngOnInit() {

		// Get cities && regions from the server
		this.getCitiesFromServer();
		this.getRegionsFromServer();

		// Get engineer from the server
		this.getEngineer();
		
	}

	getEngineer(): void {
		// Show the loader
		this._loader.show();
		// Get the engineer id from he url
		const engineer_id: number = parseInt(this._route.snapshot.paramMap.get('id'));
		// Subscribe to the service
		this._engineersService.getEngineer(engineer_id).subscribe(
			(engineer) => {

				// Create an object Engineer
				this.engineer = new Engineer(
					engineer.id,
					new User(engineer.user.id, engineer.user.avatar, engineer.user.username, engineer.user.email, engineer.user.phone, engineer.user.first_name, engineer.user.last_name, engineer.user.cin, engineer.user.address, engineer.user.city, engineer.user.zip_code, engineer.user.role, engineer.user.status, engineer.user.creation_date),
					engineer.work_region,
					engineer.work_cities
				);

				// Set form variables
				this.user = new User(engineer.user.id, engineer.user.avatar, engineer.user.username, engineer.user.email, engineer.user.phone, engineer.user.first_name, engineer.user.last_name, engineer.user.cin, engineer.user.address, engineer.user.city, engineer.user.zip_code, engineer.user.role, engineer.user.status, engineer.user.creation_date);
				this.cityId = engineer.user.city.id.toString();
				this.regionId = engineer.work_region.id.toString();
				this.workCitiesIds = engineer.work_cities_ids.split(",");

				//this.getWorkCitiesFromServer();

				console.log("cityId: ", this.cityId)
				console.log("regionId: ", this.regionId)
				console.log("workCitiesIds: ", this.workCitiesIds)

				// Hide loader
				this._loader.hide();
			}, (error) => {
				console.log("getEngineer() failed: ", error);
			}
		);
	}

	goBack(): void {
		this._location.back();
	}

	getCities(): City[] {
		return this.cities;
	}

	getRegions(): Region[] {
		return this.regions;
	}

	getWorkCities(): City[] {
		return this.workCities;
	}

	getCitiesFromServer(): void {
		// Subscribe to the engineers service
		this._citiesService.getCities().subscribe(
			(response) => {
				this.cities = response.map(city => {
					return new City(
						city.id,
						city.name,
						new Region(city.region.id, city.region.name)
					);
				});

				this.getWorkCitiesFrom();
			}, (error) => {
				console.log("getCities() failed: ", error);
			}
		);
	}

	getRegionsFromServer(): void {
		// Subscribe to the engineers service
		this._regionsService.getRegions().subscribe(
			(response) => {
				this.regions = response.map(region => {
					return new Region(
						region.id,
						region.name
					);
				});
			}, (error) => {
				console.log("getRegions() failed: ", error);
			}
		);
	}

	getWorkCitiesFrom(): void {
		this.workCitiesIds = [];
		// Subscribe to the engineers service
		this.workCities = this.cities.filter(city => city.region.id == Number.parseInt(this.regionId));
	}

	getWorkCitiesFromServer(): void {
		// Subscribe to the engineers service
		this._engineersService.getWorkCities(Number.parseInt(this.regionId)).subscribe(
			(response) => {
				this.workCitiesIds = [];
				this.workCities = response.map(city => {
					return new City(
						city.id,
						city.name,
						new Region(city.region.id, city.region.name)
					);
				});
			}, (error) => {
				console.log("getCities() failed: ", error);
			}
		);
	}

	cinAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkCin(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	usernameAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkUsername(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	emailAsyncValidator = (control: FormControl) => Observable.create((observer: Observer<ValidationErrors>) => {
		// Subscribe to the engineers service
		this._usersService.checkEmail(control.value).subscribe(
			(response) => {
				if (response) {
					observer.next({ error: true, duplicated: true });
				} else {
					observer.next(null);
				}
				observer.complete();
			}, (error) => {
				console.log("cinAsyncValidator() failed: ", error);
			}
		);
	});

	validateConfirmPassword(): void {
		setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
	}

	confirmValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
	}

	updateEngineer(engineer: Engineer): void {

		// Subscribe to the users service
		this._usersService.updateClient(engineer.user).subscribe(
			(user) => {
				engineer.user.id = user.id;
				// Subscribe to the engineers service
				this._engineersService.updateEngineer(engineer).subscribe(
					(engineer) => {
						this.goBack();
						this._loader.hide();
					}, (error) => {
						console.log("createEngineer() failed: ", error);
						this._loader.hide();
					}
				);

			}, (error) => {
				console.log("createUser() failed: ", error);
				this._loader.hide();
			}
		);

	}

	submitForm = ($event, value) => {

		// Validation
		$event.preventDefault();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}

		// Show the loader
		this._loader.show();

		// Set Engineer avatar
		//this.user.avatar = this.selectedFiles.item(0).name;

		// Set Engineer city
		for (let i = 0; i < this.cities.length; i++) {
			if (this.cities[i].id == Number.parseInt(this.cityId)) {
				this.user.city = new City(this.cities[i].id, this.cities[i].name, this.cities[i].region);
				break;
			}
		}

		// Set engineer role
		this.user.role = new Role(3);

		// Set engineer infos
		this.engineer.user = this.user;

		// Set work region
		for (let i = 0; i < this.regions.length; i++) {
			if (this.regions[i].id == Number.parseInt(this.regionId)) {
				this.engineer.work_region = new Region(this.regions[i].id, this.regions[i].name);
				break;
			}
		}

		// Set work cities
		var workCitiesIdsString: string = "";
		this.workCitiesIds = this.workCitiesIds.sort((a, b) => {
			if (a < b)
				return 1;
			else
				return -1;
		})
		this.workCitiesIds.forEach(workCityId => {
			workCitiesIdsString += workCityId.toString() + ",";
		});
		this.engineer.work_cities_ids = workCitiesIdsString.substr(0, workCitiesIdsString.length - 1);

		// Create the engineer
		// this.updateEngineer(this.engineer);
		console.log("cityId: ", this.cityId)
		console.log("regionId: ", this.regionId)
		console.log("workCitiesIds: ", this.workCitiesIds)

	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset();
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
	}

	// Upload the engineer's avatar
	selectedFiles: FileList;
	currentFileUpload: File;
	progress: { percentage: number } = { percentage: 0 };

	selectFile(event) {
		this.selectedFiles = event.target.files;
	}

	upload() {
		this.progress.percentage = 0;

		console.log(this.selectedFiles)

		this.currentFileUpload = this.selectedFiles.item(0);

		this._engineersService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
			if (event.type === HttpEventType.UploadProgress) {
				this.progress.percentage = Math.round(100 * event.loaded / event.total);
			} else if (event instanceof HttpResponse) {
				console.log('File is completely uploaded!');
			}
		});

		this.selectedFiles = undefined;
	}

}
