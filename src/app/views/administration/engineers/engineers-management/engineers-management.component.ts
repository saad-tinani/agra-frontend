import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { EngineersFilterPipe } from 'src/app/filters/engineers-filters/engineers-filter.pipe';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { Engineer } from 'src/app/models/engineer.model';
import { EngineersService } from 'src/app/services/engineers.service';
import { CitiesService } from 'src/app/services/cities.service';
import { City } from 'src/app/models/city.model';
import { ClientsService } from 'src/app/services/clients.service';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-engineers-management',
	templateUrl: './engineers-management.component.html',
	styleUrls: ['./engineers-management.component.css']
})
export class EngineersManagementComponent implements OnInit {

	// Engineers table
	private engineers: Engineer[];

	// Search
	private searchText: string;
	private engineersFilterPipe = new EngineersFilterPipe();

	constructor(private _location: Location,
		private _engineersService: EngineersService,
		private _clientsService: ClientsService,
		private _citiesService: CitiesService,
		private _authService: AuthService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService) { }

	ngOnInit() {

		// Get engineers from the server
		this.getEngineers();

	}

	goBack(): void {
		this._location.back();
	}

	getEngineersNumber(): number {
		return this.engineersFilterPipe.transform(this.engineers, this.searchText).length;
	}

	getEngineers(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		if (this._authService.getUser().role.id == 1) {
			this._engineersService.getEngineers().subscribe(
				(response) => {
					this.engineers = response.map(engineer => {
						var cities_ids: number[] = engineer.work_cities_ids.split(",").map(Number);
						var work_cities: City[] = this.getWorkCities(cities_ids);
						return new Engineer(
							engineer.id,
							new User(engineer.user.id, engineer.user.avatar, engineer.user.username, engineer.user.email, engineer.user.phone, engineer.user.first_name, engineer.user.last_name, engineer.user.cin, engineer.user.address, engineer.user.city, engineer.user.zip_code, engineer.user.role, engineer.user.status, engineer.user.creation_date),
							engineer.work_region,
							work_cities
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getEngineers() failed: ", error);
					this._loader.hide();
				}
			);
		} else if (this._authService.getUser().role.id == 2) {
			this._engineersService.getDirectorEngineers(this._authService.getDirector().work_region.id).subscribe(
				(response) => {
					this.engineers = response.map(engineer => {
						var cities_ids: number[] = engineer.work_cities_ids.split(",").map(Number);
						var work_cities: City[] = this.getWorkCities(cities_ids);
						return new Engineer(
							engineer.id,
							new User(engineer.user.id, engineer.user.avatar, engineer.user.username, engineer.user.email, engineer.user.phone, engineer.user.first_name, engineer.user.last_name, engineer.user.cin, engineer.user.address, engineer.user.city, engineer.user.zip_code, engineer.user.role, engineer.user.status, engineer.user.creation_date),
							engineer.work_region,
							work_cities
						);
					});
					this._loader.hide();
				}, (error) => {
					console.log("getEngineers() failed: ", error);
					this._loader.hide();
				}
			);
		}
	}

	getWorkCities(cities_ids: number[]): City[] {
		var cities: City[] = new Array();
		cities_ids.forEach(city_id => {
			this._citiesService.getCity(city_id).subscribe(
				(city) => {
					cities.push(new City(city.id, city.name, city.region));
				}
			);
		}, (error) => {
			console.log("getWorkCities() failed: ", error);
		});
		return cities;
	}

	updateEngineer(engineer: Engineer): void {
		// Show the loader
		this._loader.show();
		// Update the user first
		this._clientsService.updateClient(engineer.user).subscribe(
			() => {
				// Subscribe to the service
				this._engineersService.updateEngineer(engineer).subscribe(
					() => {
						this._loader.hide();
						this.goBack();
					},
					(error) => {
						console.log("updateEngineer() failed: ", error);
					}
				);
			},
			(error) => {
				console.log("updateEngineer(USER) failed: ", error);
			}
		);
	}

	changeEngineerStatus(engineer: Engineer) {
		// Show the loader
		this._loader.show();
		// Change the engineer status
		engineer.user.status = !engineer.user.status;
		// Subscribe to the service
		this._clientsService.updateClient(engineer.user).subscribe(
			() => {
				// Update engineers table
				this.engineers.forEach(element => {
					element = element.id == engineer.id ? engineer : element;
				});
				this._loader.hide();
			},
			(error) => {
				console.log("changeEngineerStatus() failed: ", error);
			}
		);
	}

	changeEngineerStatusConfirmation(engineer: Engineer, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.changeEngineerStatus(engineer),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
