import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineersManagementComponent } from './engineers-management.component';

describe('EngineersManagementComponent', () => {
  let component: EngineersManagementComponent;
  let fixture: ComponentFixture<EngineersManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngineersManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineersManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
