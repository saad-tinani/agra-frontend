import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEngineerComponent } from './new-engineer.component';

describe('NewEngineerComponent', () => {
  let component: NewEngineerComponent;
  let fixture: ComponentFixture<NewEngineerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEngineerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEngineerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
