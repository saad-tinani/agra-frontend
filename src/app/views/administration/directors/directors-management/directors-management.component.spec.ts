import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectorsManagementComponent } from './directors-management.component';

describe('DirectorsManagementComponent', () => {
  let component: DirectorsManagementComponent;
  let fixture: ComponentFixture<DirectorsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectorsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectorsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
