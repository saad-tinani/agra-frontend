import { Component, OnInit } from '@angular/core';
import { Director } from 'src/app/models/director.model';
import { DirectorsFilterPipe } from 'src/app/filters/directors-filters/directors-filter.pipe';
import { NgxSpinnerService } from 'ngx-spinner';
import { NzModalService } from 'ng-zorro-antd';
import { DirectorsService } from 'src/app/services/directors.service';
import { Location } from '@angular/common';
import { ClientsService } from 'src/app/services/clients.service';

@Component({
	selector: 'app-directors-management',
	templateUrl: './directors-management.component.html',
	styleUrls: ['./directors-management.component.css']
})
export class DirectorsManagementComponent implements OnInit {

	// Directors table
	private directors: Director[];

	// Search
	private searchText: string;
	private directorsFilterPipe = new DirectorsFilterPipe();

	constructor(
		private _location: Location,
		private _directorsService: DirectorsService,
		private _clientsService: ClientsService,
		private _loader: NgxSpinnerService,
		private _modalService: NzModalService
	) { }

	ngOnInit() {
		this.getDirectors();
	}

	goBack(): void {
		this._location.back();
	}

	getDirectorsNumber(): number {
		return this.directorsFilterPipe.transform(this.directors, this.searchText).length;
	}

	getDirectors(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._directorsService.getDirectors().subscribe(
			(response) => {
				this.directors = response.map(director => {
					return new Director(
						director.id,
						director.user,
						director.work_region
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getDirectors() failed: ", error);
				this._loader.hide();
			}
		);
	}

	updateDirector(director: Director): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._directorsService.updateDirector(director).subscribe(
			() => {
				this._loader.hide();
				this.goBack();
			},
			(error) => {
				console.log("updateDirector() failed: ", error);
			}
		);
	}

	changeDirectorStatus(director: Director) {
		// Show the loader
		this._loader.show();
		// Change the engineer status
		director.user.status = !director.user.status;
		// Subscribe to the service
		this._clientsService.updateClient(director.user).subscribe(
			() => {
				// Update engineers table
				this.directors.forEach(element => {
					element = element.id == director.id ? director : element;
				});
				this._loader.hide();
			},
			(error) => {
				console.log("changeDirectorStatus() failed: ", error);
			}
		);
	}

	changeDirectorStatusConfirmation(director: Director, action: string): void {
		this._modalService.confirm({
			nzOkType: 'danger',
			nzTitle: action,
			nzContent: '<b style="color: red;"></b>',
			nzOkText: 'Oui',
			nzOnOk: () => this.changeDirectorStatus(director),
			nzCancelText: 'Annuler',
			nzOnCancel: () => console.log('َAnnulé')
		});
	}

}
