import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-store-structure',
	templateUrl: './store-structure.component.html',
	styleUrls: ['./store-structure.component.css']
})
export class StoreStructureComponent implements OnInit {

	constructor(private _router: Router) { }

	ngOnInit() {
	}

	get getSideBar(): boolean {

		if(this._router.url != "/magasin/panier") {
			return true;
		} else {
			return false;
		}

	}

}
