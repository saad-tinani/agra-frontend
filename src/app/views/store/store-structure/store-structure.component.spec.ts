import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreStructureComponent } from './store-structure.component';

describe('StoreStructureComponent', () => {
  let component: StoreStructureComponent;
  let fixture: ComponentFixture<StoreStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
