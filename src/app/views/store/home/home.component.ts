import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Product } from 'src/app/models/product.model';
import { ShoppingCart } from 'src/app/services/shopping-cart.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	// Product table
	private products: Product[] = [];

	constructor(private _shoppingCartService: ShoppingCart,
		private _productsService: ProductsService,
		private _loader: NgxSpinnerService) { }

	ngOnInit() {

		this.getProductsFromServer();

	}

	getProducts(): Product[] {
		return this.products.sort((a, b) => {
			if (a.id < b.id)
				return 1;
			else
				return -1;
		});;
	}

	// Get products from the server
	getProductsFromServer(): void {
		// Show the loader
		this._loader.show();
		// Subscribe to the service
		this._productsService.getProducts().subscribe(
			(response) => {
				this.products = response.map(product => {
					return new Product(
						product.id,
						product.category,
						product.name,
						product.picture,
						product.description,
						product.unit_price,
						product.quantity,
						product.region,
						product.status,
						product.creation_date
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getProducts() failed: ", error);
				this._loader.hide();
			}
		);
	}

	// Add product to shoppingcart
	addToShoppingCart(product: Product, quantity: number = 1): void {
		this._shoppingCartService.addToShoppingCart(product, quantity);
	}

}
