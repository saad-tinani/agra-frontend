import { Component, OnInit } from '@angular/core';
import { ShoppingCart } from 'src/app/services/shopping-cart.service';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { Product } from 'src/app/models/product.model';
import { Order } from 'src/app/models/order.model';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	constructor(private _shoppingCartService: ShoppingCart,
		private _ordersService: OrdersService,
		private _authService: AuthService,
		private _router: Router) { }

	ngOnInit() {
	}

	// Place order
	placeOrder(): void {

		var _order: Order = new Order(1, this.generateReference(), this._authService.getUser());

		this._ordersService.placeOrder(_order).subscribe(
			(response) => {

				var shoppingCartContent: OrderDetail[] = this._shoppingCartService.getShoppingCartContent();

				shoppingCartContent.forEach(item => {
					item.order.id = response.id;
				});

				this._ordersService.placeOrderDetails(shoppingCartContent).subscribe(
					(response) => {
						this._shoppingCartService.setShoppingCartContent([]);
						this._router.navigate(["/"]);
					},
					(error) => {
						console.log("updateEngineer(USER) failed: ", error);
					}
				);

			},
			(error) => {
				console.log("updateEngineer(USER) failed: ", error);
			}
		);
	}

	// Place order details
	placeOrderDetails(orderDetails: OrderDetail[]): void {
		this._ordersService.placeOrderDetails(orderDetails);
	}

	generateReference(): string {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	  
		for (var i = 0; i < 10; i++)
		  text += possible.charAt(Math.floor(Math.random() * possible.length));
	  
		return text;
	}

	// Remove product from shoppingcart
	removeFromShoppingCart(product: Product, quantity: number = 1): void {
		this._shoppingCartService.removeFromShoppingCart(product, quantity);
	}

	getShoppingCartContent(): OrderDetail[] {
		return this._shoppingCartService.getShoppingCartContent();
	}

	getItemsNumber(): number {
		return this._shoppingCartService.getItemsNumber();
	}

	getPrixHT(): number {
		return this._shoppingCartService.getPrixHT();
	}

	getPrixTVA(): number {
		return this._shoppingCartService.getPrixTVA();
	}

	getPrixTTC(): number {
		return this._shoppingCartService.getPrixTTC();
	}

}
