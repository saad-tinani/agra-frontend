import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import { Product } from 'src/app/models/product.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { ShoppingCart } from 'src/app/services/shopping-cart.service';
import { Category } from 'src/app/models/category.model';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

	private category: Category = new Category();
	private products: Product[] = [];

	constructor(private _shoppingCartService: ShoppingCart,
		private _categoriesService: CategoriesService,
		private _productsService: ProductsService,
		private _loader: NgxSpinnerService,
		private _route: ActivatedRoute) { }

	ngOnInit() {

		// Get Category
		this.getCategoryFromServer();

		// Get products
		this.getCategoryProducts();

	}

	getCategory(): Category {
		return this.category;
	}

	getProducts(): Product[] {
		return this.products;
	}

	getCategoryFromServer(): void {
		const category_id: number = parseInt(this._route.snapshot.paramMap.get('id'));
		this._categoriesService.getCategory(category_id).subscribe(
			(response) => {
				this.category = new Category(
					response.id,
					response.name,
					response.description,
					response.position,
					response.status,
					response.creation_date
				);
			}, (error) => {
				console.log("getProducts() failed: ", error);
			}
		);
	}

	// Get category products
	getCategoryProducts(): void {
		// Show the loader
		this._loader.show();
		const category_id: number = parseInt(this._route.snapshot.paramMap.get('id'));
		// Subscribe to the service
		this._productsService.getCategoryProducts(1).subscribe(
			(response) => {
				this.products = response.map(product => {
					return new Product(
						product.id,
						product.category,
						product.name,
						product.picture,
						product.description,
						product.unit_price,
						product.quantity,
						product.region,
						product.status,
						product.creation_date
					);
				});
				this._loader.hide();
			}, (error) => {
				console.log("getProducts() failed: ", error);
			}
		);
	}

	// Add product to shoppingcart
	addToShoppingCart(product: Product, quantity: number = 1): void {
		this._shoppingCartService.addToShoppingCart(product, quantity);
	}

}
