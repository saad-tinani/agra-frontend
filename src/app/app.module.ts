import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, RoutingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopNavBarComponent } from './components/administration/top-nav-bar/top-nav-bar.component';
import { SideNavBarComponent } from './components/administration/side-nav-bar/side-nav-bar.component';
import { AdminFooterComponent } from './components/administration/admin-footer/admin-footer.component';
import { HeaderComponent } from './components/store/header/header.component';
import { SideBarComponent } from './components/store/side-bar/side-bar.component';
import { FooterComponent } from './components/store/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ClientsFilterPipe } from './filters/clients-filters/clients-filter.pipe';
import { EngineersFilterPipe } from './filters/engineers-filters/engineers-filter.pipe';
import { DirectorsFilterPipe } from './filters/directors-filters/directors-filter.pipe';
import { RolesFilterPipe } from './filters/roles-filters/roles-filter.pipe';
import { ProductsFilterPipe } from './filters/products-filters/products-filter.pipe';
import { OrdersFilterPipe } from './filters/orders-filters/orders-filter.pipe';

import { TypeaheadModule } from 'ngx-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';

import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
registerLocaleData(en);

@NgModule({
	declarations: [
		AppComponent,
		RoutingComponents,
		ClientsFilterPipe,
		EngineersFilterPipe,
		DirectorsFilterPipe,
		RolesFilterPipe,
		ProductsFilterPipe,
		OrdersFilterPipe,
		TopNavBarComponent,
		SideNavBarComponent,
		AdminFooterComponent,
		HeaderComponent,
		SideBarComponent,
		FooterComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		TypeaheadModule.forRoot(),
		NgxSpinnerModule,
		NgZorroAntdModule
	],
	providers: [
		{ provide: NZ_I18N, useValue: en_US }
	],
	bootstrap: [AppComponent]
})

export class AppModule { }
