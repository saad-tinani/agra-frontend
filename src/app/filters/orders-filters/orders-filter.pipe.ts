import { Pipe, PipeTransform } from '@angular/core';
import { Order } from 'src/app/models/order.model';

@Pipe({
	name: 'ordersFilter'
})
export class OrdersFilterPipe implements PipeTransform {

	transform(orders: Order[], searchText: string): any[] {

		if (!orders) {
			return [];
		}

		if (!searchText) {
			return orders;
		}

		searchText = searchText.toLocaleLowerCase();

		return orders.filter(order => {
			return order.reference.toLocaleLowerCase().includes(searchText) ||
				order.client.first_name.toLocaleLowerCase().includes(searchText) ||
				order.client.last_name.toLocaleLowerCase().includes(searchText) ||
				order.client.cin.toLocaleLowerCase().includes(searchText) ||
				order.client.username.toLocaleLowerCase().includes(searchText) ||
				order.client.email.toLocaleLowerCase().includes(searchText) ||
				order.client.phone.toLocaleLowerCase().includes(searchText) ||
				order.client.address.toLocaleLowerCase().includes(searchText) ||
				order.client.city.name.toLocaleLowerCase().includes(searchText) ||
				order.client.zip_code.toString().toLocaleLowerCase().includes(searchText);
		});

	}

}
