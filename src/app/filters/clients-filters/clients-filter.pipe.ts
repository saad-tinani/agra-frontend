import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Pipe({
	name: 'clientsFilter'
})
export class ClientsFilterPipe implements PipeTransform {

	transform(clients: User[], searchText: string): any[] {

		if (!clients) {
			return [];
		}

		if (!searchText) {
			return clients;
		}

		searchText = searchText.toLocaleLowerCase();

		return clients.filter(client => {
			return client.first_name.toLocaleLowerCase().includes(searchText) ||
				client.last_name.toLocaleLowerCase().includes(searchText) ||
				client.cin.toLocaleLowerCase().includes(searchText) ||
				client.username.toLocaleLowerCase().includes(searchText) ||
				client.email.toLocaleLowerCase().includes(searchText) ||
				client.phone.toLocaleLowerCase().includes(searchText) ||
				client.address.toLocaleLowerCase().includes(searchText) ||
				client.city.name.toLocaleLowerCase().includes(searchText) ||
				client.zip_code.toString().toLocaleLowerCase().includes(searchText);
		});

	}

}
