import { Pipe, PipeTransform } from '@angular/core';
import { Engineer } from 'src/app/models/engineer.model';
import { City } from 'src/app/models/city.model';

@Pipe({
	name: 'engineersFilter'
})
export class EngineersFilterPipe implements PipeTransform {

	transform(engineers: Engineer[], searchText: string): any[] {

		if (!engineers) {
			return [];
		}

		if (!searchText) {
			return engineers;
		}

		searchText = searchText.toLocaleLowerCase();

		return engineers.filter(engineer => {
			return engineer.user.first_name.toLocaleLowerCase().includes(searchText) ||
				engineer.user.last_name.toLocaleLowerCase().includes(searchText) ||
				engineer.user.username.toLocaleLowerCase().includes(searchText) ||
				engineer.user.email.toLocaleLowerCase().includes(searchText) ||
				engineer.user.phone.toLocaleLowerCase().includes(searchText) ||
				engineer.user.address.toLocaleLowerCase().includes(searchText) ||
				engineer.user.city.name.toLocaleLowerCase().includes(searchText) ||
				engineer.user.zip_code.toString().toLocaleLowerCase().includes(searchText) ||
				engineer.work_region.name.toLocaleLowerCase().includes(searchText) ||
				this.citiesSearch(engineer.work_cities, searchText)
		});

	}

	citiesSearch(cities: City[], city_name: string) {

		for (let city of cities) {
			if (city.name.toLocaleLowerCase().includes(city_name))
				return true;
		}

		return false;

	}

}
