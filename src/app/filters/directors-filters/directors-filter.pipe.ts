import { Pipe, PipeTransform } from '@angular/core';
import { Director } from 'src/app/models/director.model';

@Pipe({
    name: 'directorsFilter'
})
export class DirectorsFilterPipe implements PipeTransform {

    transform(directors: Director[], searchText: string): any[] {

        if (!directors) {
            return [];
        }

        if (!searchText) {
            return directors;
        }

        searchText = searchText.toLocaleLowerCase();

        return directors.filter(director => {
            return director.user.first_name.toLocaleLowerCase().includes(searchText) ||
                director.user.last_name.toLocaleLowerCase().includes(searchText) ||
                director.user.username.toLocaleLowerCase().includes(searchText) ||
                director.user.email.toLocaleLowerCase().includes(searchText) ||
                director.user.phone.toLocaleLowerCase().includes(searchText) ||
                director.user.address.toLocaleLowerCase().includes(searchText) ||
                director.user.city.name.toLocaleLowerCase().includes(searchText) ||
                director.user.zip_code.toString().toLocaleLowerCase().includes(searchText) ||
                director.work_region.name.toLocaleLowerCase().includes(searchText)
        });

    }

}
