import { Pipe, PipeTransform } from '@angular/core';
import { Role } from 'src/app/models/role.model';

@Pipe({
    name: 'rolesFilter'
})
export class RolesFilterPipe implements PipeTransform {

    transform(roles: Role[], searchText: string): any[] {

        if (!roles) {
            return [];
        }

        if (!searchText) {
            return roles;
        }

        searchText = searchText.toLocaleLowerCase();

        return roles.filter(role => {
            return role.name.toLocaleLowerCase().includes(searchText)
        });

    }

}
