import { Pipe, PipeTransform } from '@angular/core';
import { Product } from 'src/app/models/product.model';

@Pipe({
    name: 'productsFilter'
})
export class ProductsFilterPipe implements PipeTransform {

    transform(products: Product[], searchText: string): any[] {

        if (!products) {
            return [];
        }

        if (!searchText) {
            return products;
        }

        searchText = searchText.toLocaleLowerCase();

        return products.filter(product => {
            return product.id.toString().includes(searchText) ||
                product.name.toLocaleLowerCase().toLocaleLowerCase().includes(searchText) ||
                product.description.toLocaleLowerCase().includes(searchText) ||
                product.unit_price.toString().toLocaleLowerCase().includes(searchText) ||
                product.quantity.toString().toLocaleLowerCase().includes(searchText) ||
                product.region.name.toLocaleLowerCase().includes(searchText) ||
                product.category.name.toLocaleLowerCase().includes(searchText);
        });

    }

}
