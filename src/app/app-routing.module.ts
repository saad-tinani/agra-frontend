import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Administration's components
import { AdministrationStructureComponent } from './views/administration/administration-structure/administration-structure.component';
import { LoginComponent } from './views/administration/authentication/login/login.component';
import { ResetPasswordComponent } from './views/administration/authentication/reset-password/reset-password.component';
import { DashboardComponent } from './views/administration/dashboard/dashboard.component';
import { PageNotFoundComponent } from './views/administration/page-not-found/page-not-found.component';
import { ServerErrorComponent } from './views/administration/server-error/server-error.component';

// Directors
import { DirectorsManagementComponent } from './views/administration/directors/directors-management/directors-management.component';
import { NewDirectorComponent } from './views/administration/directors/new-director/new-director.component';

// Engineers
import { EngineersManagementComponent } from './views/administration/engineers/engineers-management/engineers-management.component';
import { EngineerDetailsComponent } from './views/administration/engineers/engineer-details/engineer-details.component';
import { NewEngineerComponent } from './views/administration/engineers/new-engineer/new-engineer.component';

// Clients
import { ClientsManagementComponent } from './views/administration/clients/clients-management/clients-management.component';
import { ClientDetailsComponent } from './views/administration/clients/client-details/client-details.component';
import { NewClientComponent } from './views/administration/clients/new-client/new-client.component';

// Orders
import { OrdersManagementComponent } from './views/administration/orders/orders-management/orders-management.component';
import { OrderDetailsComponent } from './views/administration/orders/order-details/order-details.component';

// Sales
import { SalesManagementComponent } from './views/administration/sales/sales-management/sales-management.component';
import { SalesDetailsComponent } from './views/administration/sales/sales-details/sales-details.component';

// Privileges
import { PrivilegesManagementComponent } from './views/administration/privileges-management/privileges-management.component';

// Products
import { ProductsManagementComponent } from './views/administration/products/products-management/products-management.component';
import { NewProductComponent } from './views/administration/products/new-product/new-product.component';

// Prices
import { PricesManagementComponent } from './views/administration/prices-management/prices-management.component';

// Store's components
import { StoreStructureComponent } from './views/store/store-structure/store-structure.component';
import { HomeComponent } from './views/store/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { SignInGuard } from './guards/sign-in.guard';
import { CategoryComponent } from './views/store/category/category.component';
import { CartComponent } from './views/store/cart/cart.component';

const routes: Routes = [
	{
		path: '', component: StoreStructureComponent, pathMatch: 'full', children: [
			{ path: '', component: HomeComponent, pathMatch: 'full' }
		]
	},
	{
		path: 'magasin', component: StoreStructureComponent, children: [
			{ path: '', component: HomeComponent, pathMatch: 'full' },
			{ path: 'accueil', component: HomeComponent },
			{ path: 'categories/:id', component: CategoryComponent },
			{ path: 'panier', component: CartComponent, canActivate: [AuthGuard] },
			{ path: '500', component: ServerErrorComponent },
			{ path: '404', component: PageNotFoundComponent },
			{ path: '**', component: PageNotFoundComponent }
		]
	},
	{ path: 'connexion', component: LoginComponent, canActivate: [SignInGuard]},
	{
		path: 'administration', component: AdministrationStructureComponent, canActivate: [AdminGuard], children: [
			{ path: '', component: DashboardComponent, pathMatch: 'full'},
			{ path: 'tableau-de-bord', component: DashboardComponent },

			{ path: 'gestion-du-personnel/gestion-des-directeurs', component: DirectorsManagementComponent },
			{ path: 'gestion-du-personnel/gestion-des-ingenieurs/directeur/:id', component: EngineerDetailsComponent },
			{ path: 'gestion-du-personnel/gestion-des-directeurs/nouveau-directeur', component: NewDirectorComponent },

			{ path: 'gestion-du-personnel/gestion-des-ingenieurs', component: EngineersManagementComponent },
			{ path: 'gestion-du-personnel/gestion-des-ingenieurs/ingenieur/:id', component: EngineerDetailsComponent },
			{ path: 'gestion-du-personnel/gestion-des-ingenieurs/nouvel-ingenieur', component: NewEngineerComponent },

			{ path: 'gestion-des-ingenieurs', component: EngineersManagementComponent },
			{ path: 'gestion-des-ingenieurs/ingenieur/:id', component: EngineerDetailsComponent },
			{ path: 'gestion-des-ingenieurs/nouvel-ingenieur', component: NewEngineerComponent },

			{ path: 'gestion-des-clients', component: ClientsManagementComponent },
			{ path: 'gestion-des-clients/client/:id', component: ClientDetailsComponent },
			{ path: 'gestion-des-clients/nouveau-client', component: NewClientComponent },
			
			{ path: 'gestion-des-produits', component: ProductsManagementComponent },
			{ path: 'gestion-des-produits/nouveau-produit', component: NewProductComponent },

			{ path: 'gestion-des-commandes', component: OrdersManagementComponent },
			{ path: 'gestion-des-commandes/commande/:id', component: OrderDetailsComponent },

			{ path: 'gestion-des-ventes', component: SalesManagementComponent },
			{ path: 'gestion-des-ventes/vente/:id', component: SalesDetailsComponent },

			{ path: 'gestion-des-tarifs', component: PricesManagementComponent },
			{ path: 'gestion-des-privileges', component: PrivilegesManagementComponent },
			{ path: '500', component: ServerErrorComponent },
			{ path: '404', component: PageNotFoundComponent },
			{ path: '**', component: PageNotFoundComponent }
		]
	},
	{ path: '500', component: ServerErrorComponent },
	{ path: '404', component: PageNotFoundComponent },
	{ path: '**', component: PageNotFoundComponent }

];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }

export const RoutingComponents = [
	// Administration's components
	AdministrationStructureComponent,
	LoginComponent,
	ResetPasswordComponent,
	DashboardComponent,
	DirectorsManagementComponent,
	NewDirectorComponent,
	EngineersManagementComponent,
	NewEngineerComponent,
	EngineerDetailsComponent,
	ClientsManagementComponent,
	NewClientComponent,
	ClientDetailsComponent,
	OrdersManagementComponent,
	SalesManagementComponent,
	SalesDetailsComponent,
	PrivilegesManagementComponent,
	ProductsManagementComponent,
	NewProductComponent,
	PricesManagementComponent,
	PageNotFoundComponent,
	ServerErrorComponent,

	// Store's components
	StoreStructureComponent,
	HomeComponent,
	CategoryComponent,
	CartComponent,
	OrderDetailsComponent
];
