import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';

declare var jQuery:any;

@Component({
	selector: 'side-nav-bar',
	templateUrl: './side-nav-bar.component.html',
	styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {

	constructor(private _authService: AuthService,
		private router: Router) { }

	ngOnInit() {
	}

	ngAfterViewInit() {
		jQuery('#side-menu').metisMenu();

		if (jQuery("body").hasClass('fixed-sidebar')) {
			jQuery('.sidebar-collapse').slimscroll({
				height: '100%'
			});
		}
	}

	activeRoute(routename: string): boolean {
		return this.router.url.indexOf(routename) > -1;
	}

	getIsLoggedIn(): boolean {
		return this._authService.getIsLoggedIn();
	}

	getCurrentUser(): User {
		return this._authService.getUser();
	}

	getRoleId(): number {
		return this._authService.getRoleId();
	}

	getPrivilegesManagementRole(): boolean {
		return this._authService.getPrivilegesManagementRole();
	}

	getDirectorsManagementRole(): boolean {
		return this._authService.getDirectorsManagementRole();
	}

	getEngineersManagementRole(): boolean {
		return this._authService.getEngineersManagementRole();
	}

	getClientsManagementRole(): boolean {
		return this._authService.getClientsManagementRole();
	}

	getProductsManagementRole(): boolean {
		return this._authService.getProductsManagementRole();
	}

	getPricesManagementRole(): boolean {
		return this._authService.getPricesManagementRole();
	}

	getSalesManagementRole(): boolean {
		return this._authService.getSalesManagementRole();
	}

	getOrdersManagementRole(): boolean {
		return this._authService.getOrdersManagementRole();
	}

}
