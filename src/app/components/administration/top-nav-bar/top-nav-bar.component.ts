import { Component, OnInit } from '@angular/core';
import { smoothlyMenu } from '../../../app.helpers';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
	selector: 'top-nav-bar',
	templateUrl: './top-nav-bar.component.html',
	styleUrls: ['./top-nav-bar.component.css']
})

export class TopNavBarComponent implements OnInit {

	constructor(private _authService: AuthService,
		private _router: Router) { }

	ngOnInit() {
	}

	toggleNavigation(): void {
		jQuery("body").toggleClass("mini-navbar");
		smoothlyMenu();
	}

	// Logout method
	logout() {
		// Call logout() method from the AuthService
		this._authService.logout();
		// Redirect to Home page
		this._router.navigate(["/"]);
	}

}
