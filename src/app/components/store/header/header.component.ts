import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { CategoriesService } from 'src/app/services/categories.service';
import { ShoppingCart } from 'src/app/services/shopping-cart.service';
import { OrderDetail } from 'src/app/models/order-detail.model';
import { Product } from 'src/app/models/product.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	private categories: Category[];

	constructor(private _categoriesService: CategoriesService,
		private _shoppingCartService: ShoppingCart,
		private _authService: AuthService) { }

	ngOnInit() {
		this.getCategoriesFromServer();
	}

	showAdministrationLink(): boolean {
		if(this._authService.getIsLoggedIn()) {
			if(this._authService.getUserRole() != 4)
				return true;
			else
				return false
		}
		return false;
	}

	IsLoggedIn(): boolean {
		return this._authService.getIsLoggedIn();
	}

	logout(): void {
		this._authService.logout();
	}

	// Remove product from shoppingcart
	removeFromShoppingCart(product: Product, quantity: number = 1): void {
		this._shoppingCartService.removeFromShoppingCart(product, quantity);
	}

	getCategoriesFromServer(): void {
		// Subscribe to the categories service
		this._categoriesService.getCategories().subscribe(
			(response) => {
				this.categories = response.map(category => {
					return new Category(
						category.id,
						category.name,
						category.description,
						category.position,
						category.status,
						category.creation_date
					);
				});
			}, (error) => {
				console.log("getCategoriesFromServer() failed: ", error);
			}
		);
	}

	getCategories(): Category[] {
		return this.categories;
	}

	getShoppingCartContent(): OrderDetail[] {
		return this._shoppingCartService.getShoppingCartContent();
	}

	getItemsNumber(): number {
		return this._shoppingCartService.getItemsNumber();
	}

	getPrixHT(): number {
		return this._shoppingCartService.getPrixHT();
	}

	getPrixTVA(): number {
		return this._shoppingCartService.getPrixTVA();
	}

	getPrixTTC(): number {
		return this._shoppingCartService.getPrixTTC();
	}

}
