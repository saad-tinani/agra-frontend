import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/app/models/category.model';

@Component({
	selector: 'side-bar',
	templateUrl: './side-bar.component.html',
	styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

	private categories: Category[];

	constructor(private _categoriesService: CategoriesService) { }

	ngOnInit() {
		this.getCategoriesFromServer();
	}

	getCategoriesFromServer(): void {
		// Subscribe to the categories service
		this._categoriesService.getCategories().subscribe(
			(response) => {
				this.categories = response.map(category => {
					return new Category(
						category.id,
						category.name,
						category.description,
						category.position,
						category.status,
						category.creation_date
					);
				});
			}, (error) => {
				console.log("getCategoriesFromServer() failed: ", error);
			}
		);
	}

	getCategories(): Category[] {
		return this.categories;
	}

}
