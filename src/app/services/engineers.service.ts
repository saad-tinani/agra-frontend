import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Engineer } from '../models/engineer.model';
import { City } from '../models/city.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class EngineersService {

	private _url: string = "http://localhost:8080/agra-backend/api/engineers/";
	private _user_url: string = "http://localhost:8080/agra-backend/api/users/";
	private _director_engineers: string = "http://localhost:8080/agra-backend/api/directorEngineers"
	private _work_cities_url: string = "http://localhost:8080/agra-backend/api/workcities/";

	private _uploadUrl: string = "http://localhost:8080/agra-backend";

	constructor(private _router: Router,
				private _httpService: HttpClient) { }

	// Get work cities from the server
	getWorkCities(region_id: number): Observable<City[]> {
		return this._httpService.get<City[]>(this._work_cities_url + region_id)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getWorkCities() method', [])
				)
			);
	}

	// Get engineers from the server
	getEngineers(): Observable<any[]> {
		return this._httpService.get<any[]>(this._url)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getEngineers() method', [])
				)
			);
	}

	// Get engineers of a region from the server
	getDirectorEngineers(region_id: number): Observable<any[]> {
		return this._httpService.post<any[]>(this._director_engineers, region_id, httpOptions)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getEngineers() method', [])
				)
			);
	}

	// Get a engineer from the server
	getEngineer(engineer_id: number): Observable<Engineer> {
		return this._httpService.get<Engineer>(this._url + engineer_id)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getEngineer() method', new Engineer())
				)
			);
	}

	// Add new engineer
	createEngineer(engineer: Engineer): Observable<Engineer> {
		return this._httpService.post<Engineer>(this._url, engineer, httpOptions)
			.pipe(
				catchError(
					this.handleError('Engineers Service - createEngineer() method', engineer)
				)
			);
	}

	// Update a engineer
	updateEngineer(engineer: Engineer): Observable<any> {
		return this._httpService.put(this._user_url + engineer.user.id, engineer.user, {responseType: 'text'})
			.pipe(
				catchError(
					this.handleError<Engineer>('Engineers Service - updateEngineer() method')
				)
			);
	}

	// Delete a engineer
	deleteEngineer(engineer: Engineer | number): Observable<Engineer> {
		const id = typeof engineer === 'number' ? engineer : engineer.id;
		const url = `${this._url}/${id}`;

		return this._httpService.delete<Engineer>(url, httpOptions)
			.pipe(
				catchError(
					this.handleError<Engineer>('Engineers Service - deleteEngineer() method')
				)
			);
	}

	pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();

		formdata.append('file', file);

		const req = new HttpRequest('POST', this._uploadUrl + '/api/uploadfile', formdata, {
			headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' }),
			reportProgress: true,
			responseType: 'text'
		});

		return this._httpService.request(req);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
