import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Role } from "../models/role.model";
import { catchError } from "rxjs/operators";
import { Region } from "../models/region.model";

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class RegionsService {

    private _regions_url: string = "http://localhost:8080/agra-backend/api/regions/";

    constructor(private _httpService: HttpClient) { }

    // Get regions from the server
	getRegions(): Observable<Region[]> {
		return this._httpService.get<Region[]>(this._regions_url)
			.pipe(
				catchError(
					this.handleError('Regions Service - getRegions() method', [])
				)
			);
	}

	// Get a city from the server
	getRegion(city_id: number): Observable<Region> {
		return this._httpService.get<Region>(this._regions_url + city_id)
			.pipe(
				catchError(
					this.handleError('Regions Service - getRegion() method', new Region())
				)
			);
    }
    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}