import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Product } from '../models/product.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class ProductsService {

	private _url: string = "http://localhost:8080/agra-backend/api/products/";
	private _region_products_url: string = "http://localhost:8080/agra-backend/api/regionProducts/";
	private _category_products_url: string = "http://localhost:8080/agra-backend/api/categoryProducts/";

	constructor(private _httpService: HttpClient) { }

	// Get products from the server
	getProducts(): Observable<Product[]> {
		return this._httpService.get<Product[]>(this._url)
			.pipe(
				catchError(
					this.handleError('Products Service - getProducts() method', [])
				)
			);
	}

	// Get products of a region from the server
	getRegionProducts(region_id: number): Observable<Product[]> {
		return this._httpService.post<Product[]>(this._region_products_url, region_id, httpOptions)
			.pipe(
				catchError(
					this.handleError('Products Service - getRegionProducts() method', [])
				)
			);
	}

	// Get a product from the server
	getProduct(product_id: number): Observable<Product> {
		return this._httpService.get<Product>(this._url + product_id)
			.pipe(
				catchError(
					this.handleError('Products Service - getProduct() method', new Product())
				)
			);
	}

	// Add new product
	createProduct(product: Product): Observable<Product> {
		return this._httpService.post<Product>(this._url, product, httpOptions)
			.pipe(
				catchError(
					this.handleError('Products Service - createProduct() method', product)
				)
			);
	}

	// Update a product
	updateProduct(product: Product): Observable<any> {
		return this._httpService.put(this._url + product.id, product, { responseType: 'text' })
			.pipe(
				catchError(
					this.handleError<Product>('Products Service - updateProducts() method')
				)
			);
	}

	// Delete a product
	deleteProduct(product: Product | number): Observable<Product> {
		const id = typeof product === 'number' ? product : product.id;
		const url = `${this._url}/${id}`;

		return this._httpService.delete<Product>(url, httpOptions)
			.pipe(
				catchError(
					this.handleError<Product>('Products Service - deleteProduct() method')
				)
			);
	}

	// Get Category products from the server
	getCategoryProducts(category_id: number): Observable<Product[]> {
		return this._httpService.get<Product[]>(this._category_products_url + category_id)
			.pipe(
				catchError(
					this.handleError('Products Service - getCategoryProducts() method', [])
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
