import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Role } from "../models/role.model";
import { catchError } from "rxjs/operators";

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class RolesService {

	private _url: string = "http://localhost:8080/agra-backend/api/roles/";

	constructor(private _httpService: HttpClient) { }

	// Get roles from the server
	getRoles(): Observable<Role[]> {
		return this._httpService.get<Role[]>(this._url)
			.pipe(
				catchError(
					this.handleError('Roles Service - getRoles() method', [])
				)
			);
	}

	// Get a role from the server
	getRole(role_id: number): Observable<Role> {
		return this._httpService.get<Role>(this._url + role_id)
			.pipe(
				catchError(
					this.handleError('Roles Service - getRole() method', new Role())
				)
			);
	}

	// Add new role
	createRole(role: Role): Observable<Role> {
		return this._httpService.post<Role>(this._url, role, httpOptions)
			.pipe(
				catchError(
					this.handleError('Roles Service - createRole() method', role)
				)
			);
	}

	// Update a role
	updateRole(role: Role): Observable<any> {
		return this._httpService.put(this._url + role.id, role, {responseType: 'text'})
			.pipe(
				catchError(
					this.handleError<Role>('Roles Service - updateRole() method')
				)
			);
	}

	// Delete a role
	deleteRole(role: Role | number): Observable<Role> {
		const id = typeof role === 'number' ? role : role.id;
		const url = `${this._url}/${id}`;

		return this._httpService.delete<Role>(url, httpOptions)
			.pipe(
				catchError(
					this.handleError<Role>('Roles Service - deleteRole() method')
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}