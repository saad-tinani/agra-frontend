import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { OrderDetail } from "../models/order-detail.model";
import { Product } from "../models/product.model";
import { Router } from "@angular/router";
import { Order } from "../models/order.model";

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class ShoppingCart {

	private shopping_cart: OrderDetail[] = [];

	private _order_url: string = "http://localhost:8080/agra-backend";

	constructor(private _httpService: HttpClient,
				private _router: Router) { }

	getShoppingCartContent(): OrderDetail[] {
		return this.shopping_cart;
	}

	setShoppingCartContent(content: OrderDetail[]) {
		this.shopping_cart = content;
	}

	// Get items number
	getItemsNumber(): number {

		var items_number: number = 0;

		if(this.shopping_cart.length > 0) {
			this.shopping_cart.forEach(item => {
				items_number += item.quantity;
			});
		}

		return items_number;

	}
	
	// Get HT
	getPrixHT(): number {

		var total: number = 0;

		if(this.shopping_cart.length > 0) {
			this.shopping_cart.forEach(item => {
				total += item.quantity * item.product.unit_price;
			});
		}

		return total;

	}

	// Get TTC
	getPrixTTC(): number {

		var total: number = 0;

		if(this.shopping_cart.length > 0) {
			this.shopping_cart.forEach(item => {
				total += item.quantity * item.product.unit_price;
			});
		}

		total += total*0.2;

		return total;

	}
	
	// Get total price
	getPrixTVA(): number {

		var total: number = 0;

		if(this.shopping_cart.length > 0) {
			this.shopping_cart.forEach(item => {
				total += item.quantity * item.product.unit_price;
			});
		}

		return total*0.2;

	}

	// Add product to shoppingcart
	addToShoppingCart(product: Product, quantity: number = 1): void {

		var isAvailabale: number = 0;

		if(this.shopping_cart.length > 0) {

			this.shopping_cart.forEach(item => {

				if(item.product.id == product.id) {

					isAvailabale++;
					item.quantity += quantity;

				}

			});

			if(isAvailabale < 1) {

				var orderDetail: OrderDetail = new OrderDetail();

				orderDetail.negociation_unit_price = product.unit_price;
				orderDetail.quantity = quantity;
				orderDetail.order = new Order();
				orderDetail.product = product;

				this.shopping_cart.push(orderDetail);

			}

		} else {

			var orderDetail: OrderDetail = new OrderDetail();

			orderDetail.negociation_unit_price = product.unit_price;
			orderDetail.quantity = quantity;
			orderDetail.order = new Order();
			orderDetail.product = product;

			this.shopping_cart.push(orderDetail);

		}

	}

	removeFromShoppingCart(product: Product, quantity: number = 1): void {

		var product_id: number;

		if(this.shopping_cart.length > 0) {

			this.shopping_cart.forEach(item => {

				if(item.product.id == product.id) {

					item.quantity -= quantity;

					if(item.quantity == 0)
						product_id = item.product.id;

				}

			});

			if(product_id) {
				this.shopping_cart = this.shopping_cart.filter(item => {
					return item.product.id != product_id;
				});

				if(this.shopping_cart.length == 0)
					this._router.navigate(["/"]);
			}
		}
	}    
    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}