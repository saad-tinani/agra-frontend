import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Engineer } from '../models/engineer.model';
import { Director } from '../models/director.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class AuthService {

    private _url: string = "http://localhost:8080/agra-backend";
    private _get_engineers_url: string = "http://localhost:8080/agra-backend/api/auth/engineers/";
    private _get_directors_url: string = "http://localhost:8080/agra-backend/api/auth/directors/";

	private isLoggedIn: boolean = JSON.parse(sessionStorage.getItem("isLoggedIn") || 'false');

	private user: User = new User();
    private engineer: Engineer = new Engineer();
    private director: Director = new Director();

	constructor(private _router: Router,
		private _httpService: HttpClient) { }

	getRoleId(): number {
		return this.user.role.id;
	}

	getPrivilegesManagementRole(): boolean {
		return this.user.role.privileges_management;
	}

	getDirectorsManagementRole(): boolean {
		return this.user.role.directors_management;
	}

	getEngineersManagementRole(): boolean {
		return this.user.role.engineers_management;
	}

	getClientsManagementRole(): boolean {
		return this.user.role.clients_management;
	}

	getProductsManagementRole(): boolean {
		return this.user.role.products_management;
	}

	getPricesManagementRole(): boolean {
		return this.user.role.prices_management;
	}

	getSalesManagementRole(): boolean {
		return this.user.role.sales_management;
	}

	getOrdersManagementRole(): boolean {
		return this.user.role.orders_management;
	}

    // Is loggned in
	getIsLoggedIn(): boolean {
		return JSON.parse(sessionStorage.getItem("isLoggedIn") || this.isLoggedIn.toString());
	}

    // Change login status
	SetIsLoggedIn(value: boolean): void {
		this.isLoggedIn = value;
	}

	getUser(): User {
		return this.user;
	}

	getUserRole(): number {
		return this.user.role.id;
	}

	setUser(user: User): void {
		this.user = user;
	}

	getEngineer(): Engineer {
        return this.engineer;
	}

	setEngineer(engineer: Engineer): void {
		this.engineer = engineer;
    }
    
    getDirector(): Director {
        return this.director;
	}

	setDirector(director: Director): void {
		this.director = director;
	}

    // Sign in
	signIn(user: any): Observable<User> {
		return this._httpService.post<User>(this._url + "/api/auth/signin", user)
			.pipe(
				// Catch the server side error
				catchError(
					this.handleError('Auth Service - signIn() method', new User())
				)
			);
    }
    
    // Get a engineer from the server
	getEngineerFromServer(engineer_id: number): Observable<Engineer> {
		return this._httpService.get<Engineer>(this._get_engineers_url + engineer_id)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getEngineer() method', new Engineer())
				)
			);
    }
    
    // Get a director from the server
	getDirectorFromServer(director_id: number): Observable<Director> {
		return this._httpService.get<Director>(this._get_directors_url + director_id)
			.pipe(
				catchError(
					this.handleError('Engineers Service - getEngineer() method', new Director())
				)
			);
	}

	// Sign up
	signUp(email: string, password: string): Observable<User> {
		return this._httpService.post<User>(this._url + '/api/auth/signup', { email, password })
			.pipe(
				// Catch the server side error
				catchError(
					this.handleError('Auth Service - signUp() method', new User())
				)
			);
	}

	logout() {
        this.SetIsLoggedIn(false);
        this.user = null;
        this.engineer = null;
        this.director = null;
        this._router.navigate(["/"]);
	}  

	handleLocationError(browserHasGeolocation, ) {
		console.log(browserHasGeolocation ?
			'Error: The Geolocation service failed.' :
			'Error: Your browser doesn\'t support geolocation.');
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
