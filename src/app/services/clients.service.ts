import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { City } from '../models/city.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class ClientsService {

	//private _url: string = "/assets/data/clients.json";
	private _clients_url: string = "http://localhost:8080/agra-backend/api/clients/";
	private _director_clients_url: string = "http://localhost:8080/agra-backend/api/directorClients/";
	private _engineer_clients_url: string = "http://localhost:8080/agra-backend/api/engineerClients/";
	private _client_url: string = "http://localhost:8080/agra-backend/api/users/";
	private _cities_url: string = "http://localhost:8080/agra-backend/api/cities/";

	private _check_cin_url: string = "http://localhost:8080/agra-backend/api/users/checkCin/";
	private _check_email_url: string = "http://localhost:8080/agra-backend/api/users/checkEmail/";
	private _check_username_url: string = "http://localhost:8080/agra-backend/api/users/checkUsername/";

	constructor(private _router: Router,
		private _httpService: HttpClient) { }

	// Check Cin
	checkCin(cin: string): Observable<boolean> {
		return this._httpService.post<boolean>(this._check_cin_url, cin, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - checkCin() method', false)
				)
			);
	}

	// Check Username
	checkUsername(username: string): Observable<boolean> {
		return this._httpService.post<boolean>(this._check_username_url, username, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - checkUsername() method', false)
				)
			);
	}

	// Check Email
	checkEmail(email: string): Observable<boolean> {
		return this._httpService.post<boolean>(this._check_email_url, email, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - checkEmail() method', false)
				)
			);
	}

	// Get clients from the server
	getClients(): Observable<User[]> {
		return this._httpService.get<User[]>(this._clients_url)
			.pipe(
				catchError(
					this.handleError('Clients Service - getClients() method', [])
				)
			);
	}

	// Get clients of a director from the server
	getDirectorClients(work_region_id: number): Observable<User[]> {
		return this._httpService.post<User[]>(this._director_clients_url, work_region_id, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - getDirectorClients() method', [])
				)
			);
	}

	// Get clients of a director from the server
	getEngineerClients(cities_ids: number[]): Observable<User[]> {
		return this._httpService.post<User[]>(this._engineer_clients_url, cities_ids, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - getEngineerClients() method', [])
				)
			);
	}

	// Get a client from the server
	getClient(client_id: number): Observable<User> {
		return this._httpService.get<User>(this._client_url + client_id)
			.pipe(
				catchError(
					this.handleError('Clients Service - getClient() method', new User())
				)
			);
	}

	// Add new client
	createClient(client: User): Observable<User> {
		return this._httpService.post<User>(this._client_url, client, httpOptions)
			.pipe(
				catchError(
					this.handleError('Clients Service - createClient() method', client)
				)
			);
	}

	// Update a client
	updateClient(client: User): Observable<any> {
		return this._httpService.put(this._client_url + client.id, client, { responseType: 'text' })
			.pipe(
				catchError(
					this.handleError<User>('Clients Service - updateClient() method', client)
				)
			);
	}

	// Delete a client
	deleteClient(client: User | number): Observable<User> {
		const id = typeof client === 'number' ? client : client.id;
		const url = `${this._client_url}/${id}`;

		return this._httpService.delete<User>(url, httpOptions)
			.pipe(
				catchError(
					this.handleError<User>('Clients Service - deleteClient() method')
				)
			);
	}

	// Get cities from the server
	getCities(): Observable<City[]> {
		return this._httpService.get<City[]>(this._cities_url)
			.pipe(
				catchError(
					this.handleError('Clients Service - getCities() method', [])
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
