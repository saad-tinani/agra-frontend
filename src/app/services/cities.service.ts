import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { City } from "../models/city.model";

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class CitiesService {

	private _cities_url: string = "http://localhost:8080/agra-backend/api/cities/";

    constructor(private _httpService: HttpClient) { }

    // Get cities from the server
	getCities(): Observable<City[]> {
		return this._httpService.get<City[]>(this._cities_url)
			.pipe(
				catchError(
					this.handleError('Cities Service - getCities() method', [])
				)
			);
	}

	// Get a city from the server
	getCity(city_id: number): Observable<City> {
		return this._httpService.get<City>(this._cities_url + city_id)
			.pipe(
				catchError(
					this.handleError('Cities Service - getCity() method', new City())
				)
			);
    }
    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}