import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { Category } from "../models/category.model";
import { Product } from "../models/product.model";

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class CategoriesService {

	private _categories_url: string = "http://localhost:8080/agra-backend/api/categories/";

    constructor(private _httpService: HttpClient) { }

    // Get categories from the server
	getCategories(): Observable<Category[]> {
		return this._httpService.get<Category[]>(this._categories_url)
			.pipe(
				catchError(
					this.handleError('Categories Service - getCategories() method', [])
				)
			);
	}

	// Get a category from the server
	getCategory(category_id: number): Observable<Category> {
		return this._httpService.get<Category>(this._categories_url + category_id)
			.pipe(
				catchError(
					this.handleError('Categories Service - getCategory() method', new Category())
				)
			);
	}
    
	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}