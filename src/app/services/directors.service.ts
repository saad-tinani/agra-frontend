import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Director } from '../models/director.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class DirectorsService {

    private _url: string = "http://localhost:8080/agra-backend/api/directors/";

	constructor(private _httpService: HttpClient) { }

	// Get directors from the server
	getDirectors(): Observable<Director[]> {
		return this._httpService.get<Director[]>(this._url)
			.pipe(
				catchError(
					this.handleError('Directors Service - getDirectors() method', [])
				)
			);
	}

	// Get a director from the server
	getDirector(director_id: number): Observable<Director> {
		return this._httpService.get<Director>(this._url + director_id)
			.pipe(
				catchError(
					this.handleError('Directors Service - getDirector() method', new Director())
				)
			);
	}

	// Add new director
	createDirector(director: Director): Observable<Director> {
		return this._httpService.post<Director>(this._url, director, httpOptions)
			.pipe(
				catchError(
					this.handleError('Directors Service - createDirector() method', director)
				)
			);
	}

	// Update a director
	updateDirector(director: Director): Observable<any> {
		return this._httpService.put(this._url + director.id, director, {responseType: 'text'})
			.pipe(
				catchError(
					this.handleError<Director>('Directors Service - updateDirector() method')
				)
			);
	}

	// Delete a director
	deleteDirector(director: Director | number): Observable<Director> {
		const id = typeof director === 'number' ? director : director.id;
		const url = `${this._url}/${id}`;

		return this._httpService.delete<Director>(url, httpOptions)
			.pipe(
				catchError(
					this.handleError<Director>('Directors Service - deleteDirector() method')
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
