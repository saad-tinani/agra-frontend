import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { OrderDetail } from '../models/order-detail.model';
import { Order } from '../models/order.model';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class OrdersService {

    private _orders_url: string = "http://localhost:8080/agra-backend";

	constructor(private _httpService: HttpClient) { }

	// Get orders from the server
	getOrders(): Observable<Order[]> {
		return this._httpService.get<Order[]>(this._orders_url + '/api/orders')
			.pipe(
				catchError(
					this.handleError('Orders Service - getOrders() method', [])
				)
			);
	}

	// Get orders of a region from the server
	getRegionOrders(region_id: number): Observable<Order[]> {
		return this._httpService.post<Order[]>(this._orders_url + '/api/regionOrders', region_id, httpOptions)
			.pipe(
				catchError(
					this.handleError('Orders Service - getRegionOrders() method', [])
				)
			);
	}

	// Update a Order
	updateOrder(order: Order): Observable<any> {
		return this._httpService.put(this._orders_url + '/api/orders/' + order.id, order, { responseType: 'text' })
			.pipe(
				catchError(
					this.handleError<Order>('Orders Service - updateOrder() method', order)
				)
			);
	}

	// Place order details
	placeOrder(order: Order): Observable<Order> {
		return this._httpService.post<Order>(this._orders_url + '/api/orders', order, httpOptions)
			.pipe(
				catchError(
					this.handleError('Orders Service - placeOrder() method', new Order())
				)
			);
    }

	// Place order details
	placeOrderDetails(orderDetails: OrderDetail[]): Observable<OrderDetail[]>{
		return this._httpService.post<OrderDetail[]>(this._orders_url + '/api/orders_details', orderDetails, httpOptions)
		.pipe(
			catchError(
				this.handleError('Orders Service - placeOrderDetails() method', [])
			)
		);
	}

	// Get order items
	getOrderItems(order_id: number): Observable<OrderDetail[]> {
		return this._httpService.get<OrderDetail[]>(this._orders_url + '/api/orders_items/' + order_id)
			.pipe(
				catchError(
					this.handleError('Orders Service - getOrderItems() method', [])
				)
			);
	}

	// Update a OrderItems
	updateOrderItems(order_details: OrderDetail[]): Observable<any> {
		return this._httpService.put(this._orders_url + '/api/orders_items/', order_details, { responseType: 'text' })
			.pipe(
				catchError(
					this.handleError('Orders Service - updateOrderItems() method', [])
				)
			);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			// this._router.navigate(["/administration/404"]);
			// (Historique) Display the error (Service - method)
			console.log(`${operation} failed: ${error.message}`);
			// Let the app keep running by returning an empty result.
			return of(result as T);

		};
	}

}
