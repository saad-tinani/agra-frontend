import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class AdminGuard implements CanActivate {

	constructor(private _authService: AuthService,
				private _router: Router) {}

	// Make a Component accessible based on a condition
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		if(!this._authService.getIsLoggedIn())
            this._router.navigate(["/connexion"]);
        else if(this._authService.getUser().role.id == 4) {
			this._router.navigate(["/"]);
			return false;
		}
		
		return this._authService.getIsLoggedIn();
	}
}
