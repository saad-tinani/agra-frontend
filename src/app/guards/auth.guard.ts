import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(private _authService: AuthService,
				private _router: Router) {}

	// Make a Component accessible based on a condition
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		if(!this._authService.getIsLoggedIn())
			this._router.navigate(["/connexion"]);
		return this._authService.getIsLoggedIn();
	}
}
