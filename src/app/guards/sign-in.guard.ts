import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class SignInGuard implements CanActivate {
	
	constructor(private _authService: AuthService,
				private _router: Router) {}

	// Make the Sign-In Components accessible based on a condition
	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		if (this._authService.getIsLoggedIn()) {
            if(this._authService.getUser().role.id == 4) {
				this._router.navigate(["/"]);
			} else {
				this._router.navigate(["/administration"]);
			}
        }
        
		return !this._authService.getIsLoggedIn();
	}
}
